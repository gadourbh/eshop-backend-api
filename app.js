const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const authJwt = require('./helpers/jwt');

app.use(cors());
app.options('*', cors())

require('dotenv/config');


//Routers
const productRouter = require('./routers/products');
const categoryRouter = require('./routers/categorys');
const userRouter = require('./routers/users');
const ordersRouter = require('./routers/orders')



//Middleware
app.use(express.json());
app.use(morgan('tiny'));
app.use(authJwt());

const api = process.env.API_URL

app.use(`${api}/products`, productRouter);
app.use(`${api}/categories`, categoryRouter);
app.use(`${api}/users`, userRouter);
app.use(`${api}/orders`, ordersRouter);


//Database
mongoose.connect(process.env.CONNECTION)
    .then(() => {
        console.log('Database Conncetion is ready...')
    })
    .catch((err) => {
        console.log(err);
    })

//Server
app.listen(3000, () => {
    console.log(api);
    console.log('server is running http://localhost:3000');
})